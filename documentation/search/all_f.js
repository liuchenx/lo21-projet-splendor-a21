var searchData=
[
  ['reduction_0',['reduction',['../class_splendor_1_1_joueur.html#a8ad29c6f8f6a1a241dd47209c4818bf2',1,'Splendor::Joueur']]],
  ['regles_1',['Regles',['../class_splendor_1_1_controleur.html#afc175d6bb976ea6fdb67c42d236ae431',1,'Splendor::Controleur::Regles()'],['../class_splendor_1_1_regles.html#a134067cf91820cfd856d13d437573ba7',1,'Splendor::Regles::Regles()'],['../class_splendor_1_1_regles.html',1,'Splendor::Regles']]],
  ['remplir_2',['remplir',['../classmateriel_1_1_pile.html#aecd530147ec28b9a3c054c994a2cddca',1,'materiel::Pile']]],
  ['reserved_3',['Reserved',['../class_splendor_1_1_joueur.html#a68625d52b220840cf99676582694ed3d',1,'Splendor::Joueur']]],
  ['reserver_4',['reserver',['../namespacemateriel.html#a0713558effa3ad23ef4f68b25854a6d9ae66faa2df4b9d6874764053216dff973',1,'materiel']]],
  ['reservercarte_5',['reserverCarte',['../class_splendor_1_1_controleur.html#a94d47186d8aa6864ab9626b10daae04d',1,'Splendor::Controleur']]],
  ['retirerjeton_6',['retirerJeton',['../classmateriel_1_1_pile.html#a25c1bc3e8077092c9f33c55a200d047e',1,'materiel::Pile']]],
  ['rouge_7',['rouge',['../classmateriel_1_1_prix.html#ad117271598efcb37f19832685753cec1',1,'materiel::Prix::rouge()'],['../namespacemateriel.html#a86468bcd739efee91f38a3c74c641479ac6ea9af480ca35cb8ded3cb033937f58',1,'materiel::rouge()']]]
];

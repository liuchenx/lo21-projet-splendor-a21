var searchData=
[
  ['nb_5fcartes_0',['nb_cartes',['../class_splendor_1_1_partie.html#a8967951d7e2830c1fae8a66e453c9d6a',1,'Splendor::Partie']]],
  ['nb_5fcartescite_1',['nb_cartesCite',['../class_splendor_1_1_partie.html#a6be244da6fadc3cef665c4b945a8fe08',1,'Splendor::Partie']]],
  ['nb_5fcartesn1_2',['nb_cartesN1',['../class_splendor_1_1_partie.html#a4135857a625edd810febc8d1d3ba428f',1,'Splendor::Partie']]],
  ['nb_5fcartesn2_3',['nb_cartesN2',['../class_splendor_1_1_partie.html#affcd1aca0b4ca1d28802c55a1c765c19',1,'Splendor::Partie']]],
  ['nb_5fcartesn3_4',['nb_cartesN3',['../class_splendor_1_1_partie.html#a30161a4fb6dc7b5d8714ff334cc86102',1,'Splendor::Partie']]],
  ['nb_5fcartesnoble_5',['nb_cartesNoble',['../class_splendor_1_1_partie.html#a5b90c9e1c0689697bf13c717fd090de5',1,'Splendor::Partie']]],
  ['nb_5fjetons_6',['nb_jetons',['../class_splendor_1_1_partie.html#af8f78bd9eb977309838782fe3f174d47',1,'Splendor::Partie']]],
  ['nbmax_7',['nbMax',['../class_splendor_1_1_plateau.html#a5796d1c256b6657b79f7a34d4fe95729',1,'Splendor::Plateau']]],
  ['noir_8',['noir',['../classmateriel_1_1_prix.html#aff4638df2cc5502cf71bc65f01436e28',1,'materiel::Prix']]],
  ['nom_9',['Nom',['../classmateriel_1_1_carte.html#a2d1f72e42f0cd5a7987045a66d137cd1',1,'materiel::Carte']]],
  ['nom_10',['nom',['../class_splendor_1_1_joueur.html#af39842d335b78295ddde3404d38cf230',1,'Splendor::Joueur']]],
  ['nombre_5fjoueurs_11',['nombre_joueurs',['../class_splendor_1_1_plateau.html#a8a5516b04f6cd076e28b291415f627c6',1,'Splendor::Plateau::nombre_joueurs()'],['../class_splendor_1_1_controleur.html#a1b8e11d75ac3339932f602e547b4f669',1,'Splendor::Controleur::nombre_joueurs()'],['../class_splendor_1_1_regles.html#aa8c872c593385f3a65d4e4c69ae1cd57',1,'Splendor::Regles::nombre_joueurs()']]]
];
